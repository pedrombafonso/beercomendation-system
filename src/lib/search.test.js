import { formatSearchString } from "./search";

describe("formatSearchString", () => {
  test("replaces spaces by underscore", () => {
    const testString = "a b";
    expect(formatSearchString(testString)).toEqual("a_b");
  });

  test("trims unnecessary spaces", () => {
    const testString = " a b ";
    expect(formatSearchString(testString)).toEqual("a_b");
  });
});
