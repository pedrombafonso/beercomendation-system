export const formatSearchString = str => str.trim().replace(/ /g, "_");
