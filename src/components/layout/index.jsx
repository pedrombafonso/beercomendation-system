import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";

import { MainBar } from "./main-bar";

const styles = {
  root: {
    flexGrow: 1
  }
};

function layout({ classes, children }) {
  return (
    <div className={classes.root}>
      <MainBar />
      {children}
    </div>
  );
}

layout.propTypes = {
  classes: PropTypes.object.isRequired
};

export const Layout = withStyles(styles)(layout);
