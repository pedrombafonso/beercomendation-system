import React from "react";
import PropTypes from "prop-types";

import Button from "@material-ui/core/Button";
import SearchIcon from "@material-ui/icons/Search";
import { withStyles } from "@material-ui/core";

const styles = theme => ({
  button: {
    margin: theme.spacing.unit
  },

  rightIcon: {
    marginLeft: theme.spacing.unit
  }
});

const button = ({ classes, ...props }) => {
  return (
    <Button
      variant="contained"
      color="secondary"
      className={classes.button}
      type="submit"
      {...props}
    >
      Search
      <SearchIcon className={classes.rightIcon} />
    </Button>
  );
};

button.propTypes = {
  classes: PropTypes.object.isRequired
};

export const SearchButton = withStyles(styles)(button);
