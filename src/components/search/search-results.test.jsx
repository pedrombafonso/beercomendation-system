import React from "react";
import renderer from "react-test-renderer";

import { SearchResults } from "./search-results";

describe("SearchResults", () => {
  it("Displays nothing", () => {
    const results = null;
    const searching = false;
    const tree = renderer
      .create(<SearchResults results={results} searching={searching} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
  it("Displays loading spinner", () => {
    const results = null;
    const searching = true;
    const tree = renderer
      .create(<SearchResults results={results} searching={searching} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
  it("Displays empty search results", () => {
    const results = [];
    const searching = false;
    const tree = renderer
      .create(<SearchResults results={results} searching={searching} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
  it("Displays multiple search results", () => {
    const results = [
      {
        id: 1,
        name: "Beer 1",
        description: "Beer 1 description",
        first_brewed: "Beer 1 first brewed"
      },
      {
        id: 2,
        name: "Beer 2",
        description: "Beer 2 description",
        first_brewed: "Beer 2 first brewed"
      }
    ];
    const searching = false;
    const tree = renderer
      .create(<SearchResults results={results} searching={searching} />)
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});
