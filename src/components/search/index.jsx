import React from "react";
import gql from "graphql-tag";

import { client } from "../../helpers/apollo";
import { Search } from "./search";

// GQL Query to retrieve REST endpoint results
const BEERS_BY_FOOD_QUERY = gql`
  query($searchInput: String!) {
    beers(search: $searchInput)
      @rest(type: "Beer", path: "beers?food={args.search}") {
      id
      name
      first_brewed
      description
    }
  }
`;

export class SearchForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      isSearching: false,
      searchResults: null,
      results: null
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    event.preventDefault();
    this.setState({ search: event.target.value, error: null });
  }

  handleSubmit(event) {
    event.preventDefault();
    const searchInput = this.state.search.replace(/ /g, "_");

    // Display loading spinner
    this.setState({ isSearching: true, results: null });
    client
      .query({
        query: BEERS_BY_FOOD_QUERY,
        variables: {
          searchInput
        }
      })
      .then(({ data: { beers } }) => {
        // Display results
        // clear loading spinner
        this.setState({ isSearching: false, results: beers });
      })
      .catch(error => {
        // Display error message
        // clear loading spinner and results
        this.setState({
          isSearching: false,
          error: error.message,
          results: null
        });
      });
  }

  render() {
    return (
      <Search
        handleSubmit={this.handleSubmit}
        handleChange={this.handleChange}
        search={this.state.search}
        error={this.state.error}
        isSearching={this.state.isSearching}
        results={this.state.results}
      />
    );
  }
}
