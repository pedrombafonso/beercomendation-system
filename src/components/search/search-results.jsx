import React from "react";
import PropTypes from "prop-types";
import CircularProgress from "@material-ui/core/CircularProgress";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
  margins: {
    position: "relative",
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

const hasResults = (searching, results) =>
  !searching && results && results.length > 0;

export const SearchResults = withStyles(styles)(
  ({ searching, results, classes }) => {
    // Display nothing
    if (!results && !searching) {
      return null;
    }
    // Display loading spinner
    if (searching) {
      return <CircularProgress />;
    }
    // Display results
    return (
      <Paper className={classes.margins}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Beer</TableCell>
              <TableCell>Description</TableCell>
              <TableCell>First brewed</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {!hasResults(searching, results) && (
              <TableRow>
                <TableCell component="th" scope="row">
                  Oops! Your food is not matching any beer...
                </TableCell>
              </TableRow>
            )}
            {hasResults(searching, results) &&
              results.map(result => (
                <TableRow key={result.id}>
                  <TableCell component="th" scope="row">
                    {result.name}
                  </TableCell>
                  <TableCell>{result.description}</TableCell>
                  <TableCell>{result.first_brewed}</TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </Paper>
    );
  }
);

SearchResults.propTypes = {
  searching: PropTypes.bool.isRequired,
  results: PropTypes.array,
  classes: PropTypes.object
};
