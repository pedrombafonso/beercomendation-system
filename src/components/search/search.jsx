import React from "react";
import PropTypes from "prop-types";
import { Typography } from "@material-ui/core";

import { SearchButton } from "./search-button";
import { SearchField } from "./search-field";
import { SearchResults } from "./search-results";

export const Search = props => {
  const {
    error,
    handleChange,
    handleSubmit,
    isSearching,
    results,
    search
  } = props;
  return (
    <React.Fragment>
      <form onSubmit={handleSubmit}>
        <SearchField onChange={handleChange} />
        <SearchButton disabled={!search} />
        {error && (
          <Typography color="error">Something's wrong: {error}</Typography>
        )}
      </form>
      <SearchResults searching={isSearching} results={results} />
    </React.Fragment>
  );
};

Search.propTypes = {
  error: PropTypes.string,
  handleChange: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  isSearching: PropTypes.bool.isRequired,
  results: PropTypes.array,
  search: PropTypes.string.isRequired
};
