import React from "react";
import renderer from "react-test-renderer";

import { SearchField } from "./search-field";

describe("SearchField", () => {
  it("renders correctly", () => {
    const tree = renderer.create(<SearchField />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
