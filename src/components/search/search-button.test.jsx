import React from "react";
import renderer from "react-test-renderer";

import { SearchButton } from "./search-button";

describe("SearchButton", () => {
  it("renders enabled", () => {
    const tree = renderer.create(<SearchButton />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("renders disabled", () => {
    const tree = renderer.create(<SearchButton disabled={true} />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
