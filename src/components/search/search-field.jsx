import React from "react";

import { Input, withStyles } from "@material-ui/core";

const styles = theme => ({
  input: {
    margin: theme.spacing.unit
  }
});

const searchField = ({ classes, onChange }) => {
  return (
    <Input
      placeholder="Enter your meal here"
      className={classes.input}
      onChange={onChange}
      inputProps={{
        "aria-label": "Add some food to get a matching beer."
      }}
    />
  );
};

export const SearchField = withStyles(styles)(searchField);
