import React, { Component } from "react";
import { ApolloProvider } from "react-apollo";
import { MuiThemeProvider } from "@material-ui/core";

import { client } from "./helpers/apollo";
import { Layout } from "./components/layout";
import { SearchForm } from "./components/search";
import { theme } from "./helpers/theme";

import "./App.css";

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <MuiThemeProvider theme={theme}>
          <div className="App">
            <Layout>
              <SearchForm />
            </Layout>
          </div>
        </MuiThemeProvider>
      </ApolloProvider>
    );
  }
}

export default App;
