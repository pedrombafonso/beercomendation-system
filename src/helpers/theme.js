import { createMuiTheme } from "@material-ui/core/styles";

// Polish flag red
const POLISH_RED = "#dc143c";

export const theme = createMuiTheme({
  palette: {
    primary: {
      main: POLISH_RED
    },
    secondary: {
      main: POLISH_RED
    }
  }
});
