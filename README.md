# Help Jacek choose a beer

## Code challenge description

1. Outline

Our colleague Jacek wants to have a fancy beer with his evening meal but he cannot decide what to choose - there are so many options! Build a simple app to advice Jacek.

2. Requirements

Use https://punkapi.com/documentation/v2 for the beer search.

Jacek does not care about yeast or hops or malt. Jacek only cares about what food the beer goes well with. The app should have a field to enter the meal, a button to
press for the search and a list that displays beers that go well with Jacek's food.

Use https://material-ui.com/ for the styles.

Jacek does not care much about the design. His only wish is so that all the elements are red like the red on the Polish flag. The search results should show the mini
mum of the beer name, the beer description and the month and the year when the beer was first brewed.

Please use React. Other than that you are free to use any libraries or boilerplates of your choosing.

## Getting Started

This section was copied from https://github.com/facebook/create-react-app

```
yarn; yarn start;
```

Runs the app in development mode.
Open http://localhost:3000 to view it in the browser.

```
yarn test;
```

Runs the test watcher in an interactive mode.
By default, runs tests related to files changed since the last commit.

```
yarn build
```

Builds the app for production to the build folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
By default, it also includes a service worker so that your app loads from local cache on future visits.

Your app is ready to be deployed.

## Built With

- [create-react-app
  ](https://github.com/facebook/create-react-app) - Create React apps with no build configuration.
- [React](https://reactjs.org/) - A JavaScript library for building user interfaces
- [Apollo GraphQL](https://www.apollographql.com/) - GraphQL client that facilitates API communication and provides REST integration
- [Material-UI](https://material-ui.com/) - React components that implement Google's Material Design
